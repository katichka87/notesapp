package myapp.com.myapplication.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import myapp.com.myapplication.R;
import myapp.com.myapplication.fragment.AbsCommonFragment;
import myapp.com.myapplication.fragment.DetailFragment;
import myapp.com.myapplication.fragment.NotesListFragment;
import myapp.com.myapplication.fragment.SearchFragment;
import myapp.com.myapplication.utils.Constants;


public class MainActivity extends AppCompatActivity {

    private AbsCommonFragment mListFragment;
    private AbsCommonFragment mDetailFragment;
    private AbsCommonFragment mSearchFragment;
    private AbsCommonFragment mSelectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getFragmentManager().addOnBackStackChangedListener(getListener());
        changeFragment(Constants.FRAGMENT_LIST, null);


        //ParseObject testObject = new ParseObject("TestObject");
        //testObject.put("foo", "bar");
        //testObject.saveInBackground();
    }

    public void changeFragment(int fragmentId, Bundle data) {
        AbsCommonFragment fragment = null;
        String tag = null;
        switch (fragmentId) {
            case Constants.FRAGMENT_LIST:
                if (mListFragment == null) {
                    mListFragment = new NotesListFragment();
                }
                mListFragment.updateArguments(data);
                fragment = mListFragment;
                tag = Constants.FRAGMENT_LIST_TAG;
                break;
            case Constants.FRAGMENT_DETAIL:
                if (mDetailFragment == null) {
                    mDetailFragment = new DetailFragment();
                }
                mDetailFragment.updateArguments(data);
                fragment = mDetailFragment;
                tag = Constants.FRAGMENT_DETAIL_TAG;
                break;
            case Constants.FRAGMENT_SEARCH:
                if (mSearchFragment == null) {
                    mSearchFragment = new SearchFragment();
                }
                mSearchFragment.updateArguments(data);
                fragment = mSearchFragment;
                tag = Constants.FRAGMENT_SEARCH_TAG;
                break;
        }
        if (fragment == null || mSelectedFragment == fragment) {
            return;
        }
        boolean useAnim = mSelectedFragment != null;

        final FragmentManager fm = getFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();

        if (!fragment.isAdded()) {
            if (useAnim) {
                ft.setCustomAnimations(R.anim.slide_left, R.anim.fade_out, 0, R.anim.slide_right);
            }
            ft.replace(R.id.fragment_container, fragment, tag)
                    //.add(R.id.fragment_container, fragment, tag)
                    .show(fragment)
                    .addToBackStack("bs")
                    .commit();

        }
        mSelectedFragment = fragment;
    }

    private FragmentManager.OnBackStackChangedListener getListener()
    {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener()
        {
            public void onBackStackChanged()
            {
                mSelectedFragment = getCurrentFragment();
            }
        };

        return result;
    }

    private AbsCommonFragment getCurrentFragment() {
        return (AbsCommonFragment) getFragmentManager().findFragmentById(R.id.fragment_container);
    }

    @Override
    public void onBackPressed() {
        AbsCommonFragment fragment = getCurrentFragment();
        if (fragment != null && fragment.goBack()) {
            return;
        }
        if (mSelectedFragment == null) {
            return;
        }
        mSelectedFragment = null;
        if (getFragmentManager().getBackStackEntryCount() == 1) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }
}
