package myapp.com.myapplication.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.ArrayList;
import java.util.List;

import myapp.com.myapplication.R;
import myapp.com.myapplication.utils.AppUtils;

public class SignUpActivity extends AppCompatActivity {

    private ViewPager mPager;
    private EditText mSignUpUserName;
    private EditText mSignUpEmail;
    private EditText mSignUpPassword;
    private EditText mSignUpRepeatPassword;
    private EditText mLoginUserName;
    private EditText mLoginPassword;
    private Button mLoginBtn;
    private Button mSignUpBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Toolbar toolbar = (Toolbar) findViewById(R.id.signup_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Login to create a note");

        mPager = (ViewPager) findViewById(R.id.pager);

        LayoutInflater inflater = LayoutInflater.from(this);
        List<View> pages = new ArrayList<View>();

        SwitchTabListener clickListener = new SwitchTabListener();

        //Add login page
        View page = inflater.inflate(R.layout.login_layout, null);
        mLoginUserName = (EditText) page.findViewById(R.id.user_name);
        mLoginPassword = (EditText) page.findViewById(R.id.password);
        mLoginBtn = (Button) page.findViewById(R.id.login);
        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
        View v = page.findViewById(R.id.signup_link);
        v.setOnClickListener(clickListener);
        pages.add(page);

        //Add signup page
        page = inflater.inflate(R.layout.signup_layout, null);
        v = page.findViewById(R.id.login_link);
        v.setOnClickListener(clickListener);
        mSignUpUserName = (EditText) page.findViewById(R.id.user_name);
        mSignUpEmail = (EditText) page.findViewById(R.id.email);
        mSignUpPassword = (EditText) page.findViewById(R.id.password);
        mSignUpRepeatPassword = (EditText) page.findViewById(R.id.repeat_password);
        mSignUpBtn = (Button) page.findViewById(R.id.sign_up);
        mSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUp();
            }
        });
        pages.add(page);

        TabsAdapter pagerAdapter = new TabsAdapter(pages);
        mPager.setAdapter(pagerAdapter);
    }

    private class SwitchTabListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.signup_link) {
                mPager.setCurrentItem(1);
            } else {
                mPager.setCurrentItem(0);
            }
        }
    }

    public class TabsAdapter extends PagerAdapter {

        List<View> pages = null;

        public TabsAdapter(List<View> pages){
            this.pages = pages;
        }

        @Override
        public Object instantiateItem(View collection, int position){
            View v = pages.get(position);
            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public void destroyItem(View collection, int position, Object view){
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public int getCount(){
            return pages.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object){
            return view.equals(object);
        }

        @Override
        public void finishUpdate(View arg0){
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1){
        }

        @Override
        public Parcelable saveState(){
            return null;
        }

        @Override
        public void startUpdate(View arg0){
        }
    }


    private boolean invalidSignUpFields(String email, String username, String password, String repeatPassword) {

        if (!(!AppUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
            AppUtils.showToast(this, "Invalid e-mail");
            return true;
        }

        if (AppUtils.isEmpty(username)) {
            AppUtils.showToast(this, "Invalid user name");
            return true;
        }

        if (AppUtils.isEmpty(password) || !password.equals(repeatPassword)) {
            AppUtils.showToast(this, "Invalid password");
            return true;
        }

        return false;
    }

    private void signUp() {
        String username = mSignUpUserName.getText().toString();
        String email = mSignUpEmail.getText().toString();
        String password = mSignUpPassword.getText().toString();
        String repeatPassword = mSignUpRepeatPassword.getText().toString();
        if (invalidSignUpFields(email, username, password, repeatPassword)) {
            return;
        }

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Signing up");
        dialog.show();

        // Set up a new Parse user
        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                dialog.dismiss();
                if (e != null) {
                    AppUtils.showToast(SignUpActivity.this, e.getMessage());
                } else {
                    AppUtils.showToast(SignUpActivity.this, "Signed up successfully");
                    startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                    finish();
                }
            }
        });
    }

    private void login() {
        String username = mLoginUserName.getText().toString();
        String password = mLoginPassword.getText().toString();
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Logging in...");
        dialog.show();
        // Call the Parse login method
        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                dialog.dismiss();
                if (e != null) {
                    AppUtils.showToast(SignUpActivity.this, e.getMessage());
                } else {
                    startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                    finish();
                }
            }
        });
    }
}
