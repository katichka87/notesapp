package myapp.com.myapplication.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import myapp.com.myapplication.activities.MainActivity;

/**
 * Created by Ekaterina on 27.10.2015.
 */
public abstract class AbsCommonFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected int getMenuResource() {
        return -1;
    }

    protected void menuItemSelected(int menuId) {
    }

    protected void setToolBar() {
        Toolbar toolbar = getToolbar();
        if (toolbar == null) {
            return;
        }
        int menuRes = getMenuResource();
        if (menuRes != -1) {
            toolbar.inflateMenu(menuRes);
        }
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                menuItemSelected(menuItem.getItemId());
                return false;
            }
        });
        //AppCompatActivity activity = (AppCompatActivity) getActivity();
        //activity.setSupportActionBar(toolbar);
        //activity.getSupportActionBar().setDisplayHomeAsUpEnabled(hasBackButton());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected boolean hasBackButton() {
        return true;
    }

    protected void onBackPressed() {
        getActivity().onBackPressed();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolBar();
    }

    protected abstract Toolbar getToolbar();

    public boolean goBack() {
        return false;
    }

    public void updateArguments(Bundle data) {

    }

    protected void changeFragment(int fragmentId, Bundle data) {
        ((MainActivity) getActivity()).changeFragment(fragmentId, data);
    }

}
