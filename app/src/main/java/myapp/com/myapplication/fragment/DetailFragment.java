package myapp.com.myapplication.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import myapp.com.myapplication.R;
import myapp.com.myapplication.utils.AppUtils;
import myapp.com.myapplication.utils.Constants;

/**
 * Created by Ekaterina on 02.11.2015.
 */
public class DetailFragment extends AbsCommonFragment {

    private EditText mNoteTitle;
    private EditText mNoteText;
    private ProgressDialog mProgress;
    private boolean mEditMode = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);
        mNoteTitle = (EditText) view.findViewById(R.id.note_title);
        mNoteText = (EditText) view.findViewById(R.id.note_text);

        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Saving");
        mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgress.setIndeterminate(true);
        return view;
    }

    protected int getMenuResource() {
        return R.menu.details_options;
    }

    protected void menuItemSelected(int menuId) {
        switch (menuId) {
            case R.id.action_done:
                final ParseUser user = ParseUser.getCurrentUser();
                if (user == null) {
                    AppUtils.showToast(getActivity(), "Please relogin");
                    return;
                }
                String noteTitle = mNoteTitle.getText().toString();
                String noteText = mNoteText.getText().toString();
                if (AppUtils.isEmpty(noteTitle) && AppUtils.isEmpty(noteText)) {
                    AppUtils.showToast(getActivity(), "The note is empty");
                    return;
                }

                ParseObject note = new ParseObject(Constants.OBJECT_NOTE);
                note.put(Constants.FIELD_USER, user);
                note.put(Constants.FIELD_TITLE, noteTitle);
                note.put(Constants.FIELD_TEXT, noteText);
                ParseACL acl = new ParseACL();
                acl.setPublicReadAccess(true);
                acl.setPublicWriteAccess(true);
                note.setACL(acl);
                mProgress.show();
                final Context context = getActivity().getApplicationContext();
                note.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (mProgress.isShowing()) {
                            mProgress.dismiss();
                        }
                        if (e != null) {
                            Toast.makeText(context, "Error occurred saving the note " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            String mes = mEditMode ? "The note was updated" : "The note was created";
                            Toast.makeText(context, mes, Toast.LENGTH_SHORT).show();
                        }
                        onBackPressed();
                    }
                });
                break;
        }
    }

    @Override
    protected Toolbar getToolbar() {
        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.detail_toolbar);
        toolbar.setTitle("Create a note");
        return toolbar;
    }

}
