package myapp.com.myapplication.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import myapp.com.myapplication.R;
import myapp.com.myapplication.utils.AppUtils;
import myapp.com.myapplication.utils.Constants;

/**
 * Created by Ekaterina on 02.11.2015.
 */
public class NotesListFragment extends AbsListFragment {

    private RecyclerView mNotesList;
    private FloatingActionButton mFloatingActionButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notes_list_fragment, container, false);

        mNotesList = (RecyclerView) view.findViewById(R.id.list);
        mFloatingActionButton = (FloatingActionButton) view.findViewById(R.id.new_note);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(Constants.FRAGMENT_DETAIL, null);
            }
        });

        // use a linear layout manager
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mNotesList.setLayoutManager(layoutManager);

        return view;
    }

    @Override
    protected Toolbar getToolbar() {
        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.notes_list_toolbar);
        return toolbar;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateNotesList();
    }

    private void updateNotesList() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.OBJECT_NOTE)
                .orderByDescending(Constants.FIELD_CREATED_AT);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e != null) {
                    AppUtils.showToast(getActivity(), "Error occurred displaying the notes");
                    return;
                }
                NotesAdapter adapter = new NotesAdapter(objects);
                mNotesList.setAdapter(adapter);
            }
        });
    }

    public static class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {
        List<ParseObject> mNotes;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mTextView;
            public ViewHolder(View v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.note_title);
            }
        }

        public NotesAdapter(List<ParseObject> notes) {
            mNotes = notes;
        }

        @Override
        public NotesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notes_list_item, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.mTextView.setText(mNotes.get(position).getString(Constants.FIELD_TITLE));
        }

        @Override
        public int getItemCount() {
            return mNotes.size();
        }
    }
}
