package myapp.com.myapplication.utils;

/**
 * Created by Ekaterina on 02.11.2015.
 */
public interface Constants {

    //Parse objects
    String OBJECT_NOTE = "Note";

    //Parse objects fields
    String FIELD_USER = "user";
    String FIELD_TITLE = "title";
    String FIELD_TEXT = "text";
    String FIELD_CREATED_AT = "createdAt";

    //fragments
    int FRAGMENT_LIST = 1;
    int FRAGMENT_DETAIL = 2;
    int FRAGMENT_SEARCH = 3;

    //fragment tags
    String FRAGMENT_LIST_TAG = "list_fragment";
    String FRAGMENT_DETAIL_TAG = "detail_fragment";
    String FRAGMENT_SEARCH_TAG = "search_fragment";
}
